<?php
namespace Thai\OSS\Helper;

use Thai\OSS\Model\MediaStorage\File\Storage;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $useOSS = null;

    /**
     * Check whether we are allowed to use OSS as our file storage backend.
     *
     * @return bool
     */
    public function checkOSSUsage()
    {
        if (is_null($this->useOSS)) {
            $currentStorage = (int)$this->scopeConfig->getValue(Storage::XML_PATH_STORAGE_MEDIA);
            $this->useOSS = $currentStorage == Storage::STORAGE_MEDIA_OSS;
        }
        return $this->useOSS;
    }

    public function getAccessKeyId()
    {
        return $this->scopeConfig->getValue('thai_oss/general/access_key_id');
    }

    public function getAccessKeySecret()
    {
        return $this->scopeConfig->getValue('thai_oss/general/access_key_secret');
    }

    public function getEndpoint()
    {
        return $this->scopeConfig->getValue('thai_oss/general/endpoint');
    }

    public function getBucket()
    {
        return $this->scopeConfig->getValue('thai_oss/general/bucket');
    }
}
