Thai's Alibaba OSS Extension for Magento 2
==========================================

Thai's Alibaba OSS extension for Magento 2 allows retailers to upload their catalogue and WYSIWYG images straight to Alibaba Object Storage Service.

Benefits
--------

### Easy to use

This extension is easy to use with little configuration! You only need to follow a few simple steps (and one of them is simply to create a copy of your images as a precaution) to get up and running!

### Sync all your media images

The following images are automatically saved to OSS:

* Product images
* Generated thumbnails
* WYSIWYG images
* Category images

### Magento can now scale horizontally

Complex file syncing between multiple servers is now a thing of the past with this extension. All your servers will be able to share the one OSS bucket as the single source of media.

FAQs
----

### Does this extension upload my log files?

No, the OSS extension only syncs across the media folder. You will need to find an alternative solution to store your log files.

### We did something wrong and all our images are gone! Can you restore it?

I recommend taking a backup of your media files when switching file storage systems. Unfortunately, there's nothing I can do if you somehow accidentally delete them.
