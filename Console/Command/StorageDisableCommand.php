<?php
namespace Thai\OSS\Console\Command;

use Magento\Config\Model\Config\Factory;
use Magento\Framework\App\State;
use Magento\MediaStorage\Helper\File\Storage;
use Magento\MediaStorage\Helper\File\Storage\Database;
use OSS\Core\OssException;
use OSS\OssClient;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Thai\OSS\Helper\Data;

class StorageDisableCommand extends \Symfony\Component\Console\Command\Command
{
    private $configFactory;

    private $state;

    private $helper;

    private $client;

    private $coreFileStorage;

    private $storageHelper;

    public function __construct(
        State $state,
        Factory $configFactory,
        Database $storageHelper,
        Storage $coreFileStorage,
        Data $helper
    ) {
        parent::__construct();

        $this->state = $state;
        $this->configFactory = $configFactory;
        $this->coreFileStorage = $coreFileStorage;
        $this->helper = $helper;
        $this->storageHelper = $storageHelper;
    }

    protected function configure()
    {
        $this->setName('oss:storage:disable');
        $this->setDescription('Revert to using the local filesystem as your Magento 2 file storage backend.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $errors = $this->validate($input);
        if ($errors) {
            $output->writeln('<error>' . implode('</error>' . PHP_EOL .  '<error>', $errors) . '</error>');
            return;
        }

        try {
            $this->client = new OssClient(
                $this->helper->getAccessKeyId(),
                $this->helper->getAccessKeySecret(),
                $this->helper->getEndpoint()
            );
        } catch (OssException $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            return;
        }

        if (!$this->client->doesBucketExist($this->helper->getBucket())) {
            $output->writeln('<error>The Alibaba OSS credentials you provided did not work. Please review your details and try again. You can do so using our config script.</error>');
            return;
        }

        if ($this->coreFileStorage->getCurrentStorageCode() != \Thai\OSS\Model\MediaStorage\File\Storage::STORAGE_MEDIA_OSS) {
            $output->writeln('<error>You are not using Alibaba OSS as your media file storage backend!</error>');
            return;
        }

        $output->writeln('Updating configuration to use the local filesystem.');

        $this->state->setAreaCode('adminhtml');
        $config = $this->configFactory->create();
        $config->setDataByPath('system/media_storage_configuration/media_storage', \Magento\MediaStorage\Model\File\Storage::STORAGE_MEDIA_FILE_SYSTEM);
        $config->save();
        $output->writeln(sprintf('<info>Magento now uses the local filesystem for its file backend storage.</info>'));
    }

    public function validate(InputInterface $input)
    {
        $errors = [];

        if (is_null($this->helper->getAccessKeyId())) {
            $errors[] = 'You have not provided an Alibaba OSS access key ID. You can do so using our config script.';
        }
        if (is_null($this->helper->getAccessKeySecret())) {
            $errors[] = 'You have not provided an Alibaba OSS access key secret. You can do so using our config script.';
        }
        if (is_null($this->helper->getBucket())) {
            $errors[] = 'You have not provided an Alibaba OSS bucket. You can do so using our config script.';
        }
        if (is_null($this->helper->getEndpoint())) {
            $errors[] = 'You have not provided an Alibaba OSS region. You can do so using our config script.';
        }

        return $errors;
    }
}
