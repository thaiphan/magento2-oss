<?php
namespace Thai\OSS\Console\Command;

use Magento\Config\Model\Config\Factory;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigSetCommand extends Command
{
    private $configFactory;

    private $state;

    public function __construct(
        State $state,
        Factory $configFactory
    ) {
        parent::__construct();

        $this->state = $state;
        $this->configFactory = $configFactory;
    }

    protected function configure()
    {
        $this->setName('oss:config:set');
        $this->setDescription('Allows you to set your Alibaba OSS configuration via the CLI.');
        $this->setDefinition($this->getOptionsList());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('endpoint') && !$input->getOption('bucket') && !$input->getOption('access-key-secret') && !$input->getOption('access-key-id')) {
            $output->writeln($this->getSynopsis());
            return;
        }

        $this->state->setAreaCode('adminhtml');
        $config = $this->configFactory->create();

        if (!empty($input->getOption('access-key-id'))) {
            $config->setDataByPath('thai_oss/general/access_key_id', $input->getOption('access-key-id'));
            $config->save();
        }

        if (!empty($input->getOption('access-key-secret'))) {
            $config->setDataByPath('thai_oss/general/access_key_secret', $input->getOption('access-key-secret'));
            $config->save();
        }

        if (!empty($input->getOption('bucket'))) {
            $config->setDataByPath('thai_oss/general/bucket', $input->getOption('bucket'));
            $config->save();
        }

        if (!empty($input->getOption('endpoint'))) {
            $config->setDataByPath('thai_oss/general/endpoint', $input->getOption('endpoint'));
            $config->save();
        }

        $output->writeln('<info>You have successfully updated your Alibaba OSS credentials.</info>');
    }

    public function getOptionsList()
    {
        return [
            new InputOption('access-key-id', null, InputOption::VALUE_OPTIONAL, 'a valid Alibaba OSS access key ID'),
            new InputOption('access-key-secret', null, InputOption::VALUE_OPTIONAL, 'a valid Alibaba OSS secret access key'),
            new InputOption('bucket', null, InputOption::VALUE_OPTIONAL, 'an Alibaba OSS bucket name'),
            new InputOption('endpoint', null, InputOption::VALUE_OPTIONAL, 'an Alibaba OSS endpoint, e.g. oss-us-east-1')
        ];
    }
}
