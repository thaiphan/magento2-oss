<?php
namespace Thai\OSS\Console\Command;

use Magento\Config\Model\Config\Factory;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigListCommand extends Command
{
    private $configFactory;

    private $state;

    public function __construct(
        State $state,
        Factory $configFactory
    ) {
        parent::__construct();

        $this->state = $state;
        $this->configFactory = $configFactory;
    }

    protected function configure()
    {
        $this->setName('oss:config:list');
        $this->setDescription('Lists whatever credentials for Alibaba OSS you have provided for Magento.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode('adminhtml');
        $config = $this->configFactory->create();
        $output->writeln('Here are your Alibaba OSS credentials.');
        $output->writeln('');
        $output->writeln(sprintf('Access Key ID:     %s', $config->getConfigDataValue('thai_oss/general/access_key_id')));
        $output->writeln(sprintf('Access Key Secret: %s', $config->getConfigDataValue('thai_oss/general/access_key_secret')));
        $output->writeln(sprintf('Bucket:            %s', $config->getConfigDataValue('thai_oss/general/bucket')));
        $output->writeln(sprintf('Endpoint:          %s', $config->getConfigDataValue('thai_oss/general/endpoint')));
    }
}
