<?php
namespace Thai\OSS\Model\MediaStorage\File\Storage\Database;

use Thai\OSS\Helper\Data;
use Thai\OSS\Model\MediaStorage\File\Storage\OSS;

class Plugin
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var OSS
     */
    private $storageModel;

    public function __construct(
        Data $helper,
        OSS $storageModel
    ) {
        $this->helper = $helper;
        $this->storageModel = $storageModel;
    }

    public function aroundGetDirectoryFiles($subject, $proceed, $directory)
    {
        if ($this->helper->checkOSSUsage()) {
            return $this->storageModel->getDirectoryFiles($directory);
        }
        return $proceed($directory);
    }
}
