<?php
namespace Thai\OSS\Model\MediaStorage\File\Storage;

class Plugin
{
    private $coreFileStorage;
    private $ossFactory;

    public function __construct(
        \Magento\MediaStorage\Helper\File\Storage $coreFileStorage,
        OSSFactory $ossFactory
    ) {
        $this->coreFileStorage = $coreFileStorage;
        $this->ossFactory = $ossFactory;
    }

    public function aroundGetStorageModel($subject, $proceed, $storage = null, array $params = [])
    {
        $storageModel = $proceed($storage, $params);
        if ($storageModel === false) {
            if (is_null($storage)) {
                $storage = $this->coreFileStorage->getCurrentStorageCode();
            }
            switch ($storage) {
                case \Thai\OSS\Model\MediaStorage\File\Storage::STORAGE_MEDIA_OSS:
                    $storageModel = $this->ossFactory->create();
                    break;
                default:
                    return false;
            }

            if (isset($params['init']) && $params['init']) {
                $storageModel->init();
            }
        }

        return $storageModel;
    }
}
