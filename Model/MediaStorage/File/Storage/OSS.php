<?php
namespace Thai\OSS\Model\MediaStorage\File\Storage;

use Magento\Framework\DataObject;
use Magento\MediaStorage\Helper\File\Media;
use Magento\MediaStorage\Helper\File\Storage\Database;
use OSS\Core\OssException;
use OSS\OssClient;
use Thai\OSS\Helper\Data;

class OSS extends DataObject
{
    /**
     * Store media base directory path
     *
     * @var string
     */
    protected $mediaBaseDirectory = null;

    /**
     * @var OssClient
     */
    private $client;

    private $helper;

    /**
     * Core file storage database
     *
     * @var Database
     */
    private $storageHelper;

    /**
     * @var Media
     */
    private $mediaHelper;

    /**
     * Collect errors during sync process
     *
     * @var string[]
     */
    private $errors = [];

    public function __construct(
        Data $helper,
        Media $mediaHelper,
        Database $storageHelper,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->helper = $helper;
        $this->mediaHelper = $mediaHelper;
        $this->storageHelper = $storageHelper;

        $this->client = new OssClient(
            $this->helper->getAccessKeyId(),
            $this->helper->getAccessKeySecret(),
            $this->helper->getEndpoint()
        );
    }

    /**
     * Initialisation
     *
     * @return $this
     */
    public function init()
    {
        return $this;
    }

    /**
     * Return storage name
     *
     * @return \Magento\Framework\Phrase
     */
    public function getStorageName()
    {
        return __('Alibaba OSS');
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function loadByFilename($filename)
    {
        $fail = false;
        try {
            $object = $this->client->getObject($this->getBucket(), $filename);

            if ($object) {
                $this->setData('id', $filename);
                $this->setData('filename', $filename);
                $this->setData('content', $object);
            } else {
                $fail = true;
            }
        } catch (OssException $e) {
            $fail = true;
        }

        if ($fail) {
            $this->unsetData();
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    public function clear()
    {
        return $this;
    }

    public function exportDirectories($offset = 0, $count = 100)
    {
        return false;
    }

    public function importDirectories(array $dirs = [])
    {
        return $this;
    }

    /**
     * Retrieve connection name
     *
     * @return null
     */
    public function getConnectionName()
    {
        return null;
    }

    public function exportFiles($offset = 0, $count = 100)
    {
        return false;
    }

    public function importFiles(array $files = [])
    {
        foreach ($files as $file) {
            $object = ltrim($file['directory'] . '/' . $file['filename'], '/');

            $this->client->putObject(
                $this->getBucket(),
                $object,
                $file['content']
            );
        }

        return $this;
    }

    /**
     * Save a file to Alibaba OSS.
     *
     * @param string $filename
     * @return $this
     */
    public function saveFile($filename)
    {
        $file = $this->mediaHelper->collectFileInfo($this->getMediaBaseDirectory(), $filename);

        $this->client->putObject(
            $this->getBucket(),
            $filename,
            $file['content']
        );

        return $this;
    }

    /**
     * Check whether a file exists in Alibaba OSS.
     *
     * @param string $filename
     * @return bool
     */
    public function fileExists($filename)
    {
        return $this->client->doesObjectExist($this->getBucket(), $filename);
    }

    public function copyFile($oldFilePath, $newFilePath)
    {
        $this->client->copyObject(
            $this->getBucket(),
            $oldFilePath,
            $this->getBucket(),
            $newFilePath
        );

        return $this;
    }

    /**
     * @param string $oldFilePath
     * @param string $newFilePath
     * @return $this
     */
    public function renameFile($oldFilePath, $newFilePath)
    {
        return $this->copyFile($oldFilePath, $newFilePath)
            ->deleteFile($oldFilePath);
    }

    /**
     * Delete file from Alibaba OSS
     *
     * @param string $path
     * @return $this
     */
    public function deleteFile($path)
    {
        $this->client->deleteObject($this->getBucket(), $path);

        return $this;
    }

    public function getSubdirectories($path)
    {
        $subdirectories = [];

        $prefix = $this->storageHelper->getMediaRelativePath($path);
        $prefix = rtrim($prefix, '/') . '/';

        $objects = $this->client->listObjects($this->getBucket(), [
            OssClient::OSS_DELIMITER => '/',
            OssClient::OSS_PREFIX => $prefix,
        ]);

        foreach ($objects->getPrefixList() as $object) {
            if ($object->getPrefix()) {
                $subdirectories[] = [
                    'name' => $object->getPrefix()
                ];
            }
        }

        return $subdirectories;
    }

    public function getDirectoryFiles($path)
    {
        $files = [];

        $prefix = $this->storageHelper->getMediaRelativePath($path);
        $prefix = rtrim($prefix, '/') . '/';

        $objects = $this->client->listObjects($this->getBucket(), [
            OssClient::OSS_PREFIX => $prefix,
        ]);

        foreach ($objects->getObjectList() as $object) {
            $content = $this->client->getObject($this->getBucket(), $object->getKey());

            if ($content) {
                $files[] = [
                    'filename' => $object->getKey(),
                    'content' => $content
                ];
            }
        }

        return $files;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function deleteDirectory($path)
    {
        $mediaRelativePath = $this->storageHelper->getMediaRelativePath($path);
        $prefix = rtrim($mediaRelativePath, '/') . '/';

        return $this->deleteNestedObjects($prefix);
    }

    /**
     * @param string $prefix
     * @return $this
     */
    protected function deleteNestedObjects($prefix)
    {
        $objects = $this->client->listObjects($this->getBucket(), [
            OssClient::OSS_PREFIX => $prefix,
        ]);

        foreach ($objects->getObjectList() as $object) {
            $this->deleteFile($object->getKey());
        }

        foreach ($objects->getPrefixList() as $object) {
            $this->deleteNestedObjects($object->getPrefix());
        }

        return $this;
    }

    protected function getBucket()
    {
        return $this->helper->getBucket();
    }

    /**
     * Retrieve media base directory path
     *
     * @return string
     */
    public function getMediaBaseDirectory()
    {
        if (is_null($this->mediaBaseDirectory)) {
            $this->mediaBaseDirectory = $this->storageHelper->getMediaBaseDir();
        }
        return $this->mediaBaseDirectory;
    }
}
