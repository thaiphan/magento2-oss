<?php
namespace Thai\OSS\Model\MediaStorage\File\Storage\Directory\Database;

use Thai\OSS\Helper\Data;
use Thai\OSS\Model\MediaStorage\File\Storage\OSS;

class Plugin
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var OSS
     */
    private $storageModel;

    public function __construct(
        Data $helper,
        OSS $storageModel
    ) {
        $this->helper = $helper;
        $this->storageModel = $storageModel;
    }

    public function aroundCreateRecursive($subject, $proceed, $path)
    {
        if ($this->helper->checkOSSUsage()) {
            return $this;
        }
        return $proceed($path);
    }

    public function aroundGetSubdirectories($subject, $proceed, $directory)
    {
        if ($this->helper->checkOSSUsage()) {
            return $this->storageModel->getSubdirectories($directory);
        } else {
            return $proceed($directory);
        }
    }

    public function aroundDeleteDirectory($subject, $proceed, $path)
    {
        if ($this->helper->checkOSSUsage()) {
            return $this->storageModel->deleteDirectory($path);
        } else {
            return $proceed($path);
        }
    }
}
